package budgetloc.budgetloc;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends Activity {

    private static final long delay = 2000L;
    WebView mWebView;
    String budgetLoc_url = "http://ec2-52-25-46-212.us-west-2.compute.amazonaws.com/";
    String errorpage = "file:///android_asset/error_page.html";
    ProgressBar loadingProgressBar, loadingTitle;
    private boolean mRecentlyBackPressed = false;
    private final Runnable mExitRunnable = new Runnable() {
        @Override
        public void run() {
            mRecentlyBackPressed = false;
        }
    };
    /*private Handler mExitHandler = new Handler() {
        @Override
        public void close() {

        }

        @Override
        public void flush() {

        }

        @Override
        public void publish(LogRecord logRecord) {

        }
    };*/

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mWebView = (WebView) findViewById(R.id.webView);

        // Enable Javascript
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        // Multi-windows
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        // mWebView.getSettings().setSupportMultipleWindows(true);
        mWebView.setWebViewClient(new MyWebViewClient());

        loadingProgressBar = (ProgressBar) findViewById(R.id.progressBar);

        mWebView.setWebChromeClient(new WebChromeClient() {
            // this will be called on page loading progress
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                loadingProgressBar.setProgress(newProgress);
                // hide the progress bar if the loading is complete
                if (newProgress == 100) {
                    loadingProgressBar.setVisibility(View.GONE);
                } else {
                    loadingProgressBar.setVisibility(View.VISIBLE);
                }
            }
        });
        mWebView.loadUrl(budgetLoc_url);
    }

    @Override
    public void onStart() {
        super.onStart();

        /*
        // Get tracker.
        Tracker t = ((AnalyticsSampleApp) getActivity().getApplication()).getTracker(
                TrackerName.APP_TRACKER);

        // Enable Advertising Features.
        t.enableAdvertisingIdCollection(true);

// Set screen name.
        t.setScreenName(screenName);

// Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());
        //EasyTracker.getInstance(this).activityStart(this);
        // mWebView.reload();

        */
    }

    @Override
    public void onStop() {
        super.onStop();
        //EasyTracker.getInstance(this).activityStop(this);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Check if the key event was the Back button and if there's history
        if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
            mWebView.goBack();
            // mWebView.reload();
            return true;
        }
        // If it wasn't the Back key or there's no web page history, bubble up
        // to the default
        // system behavior (probably exit the activity)
        return super.onKeyDown(keyCode, event);
    }


    @Override
    public void onBackPressed() {
        // 2 Back finishes the app
        if (mRecentlyBackPressed) {
            //mExitHandler.removeCallbacks(mExitRunnable);
            //mExitHandler = null;
            super.onBackPressed();
        } else {
            mRecentlyBackPressed = true;
            Toast.makeText(this, R.string.back_twice_message, Toast.LENGTH_SHORT).show();
            //mExitHandler.postDelayed(mExitRunnable, delay);
        }
    }


    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
            // In the WebView
            //view.loadUrl(url);
            //return true;

            // If not budgetLoc_url open browser
            //return !Uri.parse(url).getHost().contains(budgetLoc_url);
        }

        // Custom Webpage error page
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            mWebView.loadUrl(errorpage);
        }

    }
}

/*
public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
*/
